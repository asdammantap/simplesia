<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jnsrek extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form')); //load helper url 
		$this->load->library('form_validation'); //load form validation
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function jenisakun()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_jenisrek');
			$data['data']=$this->modul_jenisrek->viewjenisrek();
			$this->load->view('admin/jenisrek/list_jenis',$data);
		}
		else {
			redirect('');
		}
	}
	public function add_jnsrek()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_jenisrek');
			$data['data']=$this->modul_jenisrek->viewjenisrek();
			$this->load->view('admin/jenisrek/add_jenis',$data);
		}
		else {
			redirect('');
		}
	}
	public function savejnsrek(){
		$this->form_validation->set_rules('kd_jenisakun','Kode Jenis Akun','required');
		$this->form_validation->set_rules('desc_jenisakun','Deskripsi Jenis Akun','required');
		$data = array(
				  'kd_jenisakun' =>$this->input->post('kd_jenisakun'),
				  'desc_jenisakun' =>$this->input->post('desc_jenisakun')
				  );
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
				$this->load->model('modul_jenisrek');
				$this->modul_jenisrek->get_insertjnsrek($data); //akses model untuk menyimpan ke database
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\"><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Data " .$this->input->post('desc_jenisakun'). " Berhasil Disimpan!!</div></div>");
                redirect('jnsrek/jenisakun'); //jika berhasil maka akan ditampilkan view jenisrekening
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\"><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Data " .$this->input->post('desc_jenisakun'). " Gagal Disimpan!!</div></div>");
                redirect('jnsrek/add_jenis'); //jika gagal maka akan ditampilkan form tambah mk
	}         
    }
	public function editjenis($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_jenisrek');
		$data['data']=$this->modul_jenisrek->get_editjnsrek($id);
		$this->load->view('admin/jenisrek/edit_jenis',$data);
		}
		else {
			redirect('');
		}
	}
	function proseseditjnsrek() { 
		$this->form_validation->set_rules('kd_jenisakun','Kode Jenis Akun','required');
		$this->form_validation->set_rules('desc_jenisakun','Deskripsi Jenis Akun','required');
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
		$this->load->model('modul_jenisrek','',TRUE); 
            $this->modul_jenisrek->moduleditjnsrek(); 
             $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('jnsrek/jenisakun'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
               $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../kls/kelas'); //jika berhasil maka akan ditampilkan view matakuliah
			}
        }
	public function hapusjenis($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_jenisrek','', TRUE);
		$data['data']=$this->modul_jenisrek->hapus_jnsrek($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('jnsrek/jenisakun');
            }
		$this->load->view('admin/jenisrek/list_jenis', $data);
	}
	
}

# nama file home.php
# folder apllication/controller/