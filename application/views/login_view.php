<html>
<head>
<title>SIA - Sistem Informasi Akuntansi</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="sistem informasi manajemen akademik">
<meta name="keyword" content="simak">
<meta name="author" content="Wong Mantap">
<script src="assets/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/bootstrap-show-password.js"></script>
<script src="assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="assets/css/login.css">  
<link rel="icon" type="image/png" id="favicon"
          href="assets/image/logosinul.png"/>
</head>
<body>
    <div class="container">
	<div class="card card-container" style="margin-top:70px;">
            <img id="profile-img" class="profile-img-card" src="assets/image/logosinul.png" />
            <p id="profile-name" class="profile-name-card"></p>
			<?=$this->session->flashdata('pesan')?>
            <?php echo form_open("sync/cek_loginadmin"); ?>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="glyphicon glyphicon-user"></i>
				</span>
                <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
			</div>
                </br>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="glyphicon glyphicon-lock"></i>
				</span>
				<input type="password" id="password" class="form-control" placeholder="Password" name="passwd" required>
			</div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="methods" value="show password">Show Password
                </label>
            </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
				<?php echo form_close(); ?>
           <a href="forgot" class="forgot-password">
                Forgot the password?
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/bootstrap-show-password.js"></script>
	<script>
    $(function() {
        $('#password').password().on('show.bs.password', function(e) {
            $('#eventLog').text('On show event');
            $('#methods').prop('checked', true);
        }).on('hide.bs.password', function(e) {
                    $('#eventLog').text('On hide event');
                    $('#methods').prop('checked', false);
                });
        $('#methods').click(function() {
            $('#password').password('toggle');
        });
    });
	</script>
</body>
</html>