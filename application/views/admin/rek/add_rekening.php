<!DOCTYPE html>
<html>
<head>
<title>SIA - Sistem Informasi Akuntansi</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="sistem informasi akuntansi">
<meta name="keyword" content="sia">
<meta name="author" content="Wong Mantap">
<!-- The styles -->
    <link href="../assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="../assets/css/charisma-app.css" rel="stylesheet">
	<link href="../assets/css/bootstrap-table.css" rel="stylesheet">
    <link href="../assets/bower_components/chosen/chosen.min.css" rel="stylesheet">
    <link href="../assets/bower_components/colorbox/example3/colorbox.css" rel="stylesheet">
    <link href="../assets/bower_components/responsive-tables/responsive-tables.css" rel="stylesheet">
    <link href="../assets/css/jquery.noty.css" rel="stylesheet">
	<link href="../assets/css/jquery.iphone.toggle.css" rel="stylesheet">
    <link href="../assets/css/noty_theme_default.css" rel="stylesheet">
    <link href="../assets/css/animate.min.css" rel="stylesheet">
	<script type="text/javascript" src="../assets/js/jquery.js"></script>
    <!-- jQuery -->
    <script src="../assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
<link rel="shortcut icon" href="../assets/image/logosinul.png"/>
</head>
<body>
	<div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>SI Akuntansi</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="../main/profile">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="../main/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->
            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
				<li><a href="../main/dashboard"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Home</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i>  Setup <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../jnsrek/jenisakun"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Data Jenis Rekening</a></li>
						<li class="divider"></li>
						<li><a href="../rek/rekening"><i class="glyphicon glyphicon-new-window"></i>&nbsp;&nbsp;&nbsp;Data Perkiraan</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-tower"></i>  Transaksi <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../jurnal/umum"><i class="glyphicon glyphicon-road"></i>&nbsp;&nbsp;&nbsp;Jurnal Umum</a></li>
						<li class="divider"></li>
						<li><a href="../jurnal/kaskeluar"><i class="glyphicon glyphicon-road"></i>&nbsp;&nbsp;&nbsp;Jurnal Kas Keluar</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-paperclip"></i>  Laporan <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../report/jurnalumum"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Jurnal Umum</a></li>
						<li class="divider"></li>
                        <li><a href="../report/bukubesar"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Buku Besar</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
	<!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->
        <div id="content" class="col-lg-12 col-sm-12">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
		<li>
            <a href="../rek/rekening">Daftar Perkiraan</a>
        </li>
		<li>
            Tambah
        </li>
    </ul>
</div>
<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading">Input Data Perkiraan</div>
					<div style="margin-top:10px;"></div>
					<div class="col-md-11 alert alert-info" style="margin-left:20px;">Form Ini Digunakan Untuk Mengisi Data Perkiraan<br><i>example:Kas,Modal Awal,Investasi, Piutang, dsb</i></div>
					<div></div>
					<div class="panel-body">
					<?=$this->session->flashdata('pesan')?>
						<form action="<?=base_url()?>rek/saverek" method="post" enctype="multipart/form-data" role="form">
				<div style="float:right;margin-bottom:10px;">
						<a class="btn btn-success" href='../jnsrek/jenisakun'">
						<i class="glyphicon glyphicon-list icon-white"></i>&nbsp;&nbsp;Daftar Perkiraan</a>
				</div>
				<div class="col-lg-6">
				<div class="panel-body"></div>
				<input type="hidden" class="form-control" placeholder="" name="tgl_awal" value="<?php echo date('Y-m-d');?>">
                <div class="input-group col-md-11">
						<span class="input-group-addon">Kode</span>
						<input type="text" class="form-control" placeholder="Kode Rek" name="kd_akun">
                </div><p></p>
               <div class="input-group col-md-11">
                    <span class="input-group-addon">Deskripsi</span>
                    <input type="text" class="form-control" placeholder="Deskripsi Rek" name="desc_akun" required>
                </div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Jenis</span>
                    <select class="form-control" style="height:40px;border-radius: 0 5px 5px 0;" name="kd_jenisakun">
							<?php
										$tampil=mysql_query("SELECT * FROM tbl_jenisakun ORDER BY kd_jenisakun ASC");
										while($r=mysql_fetch_array($tampil))
										{
										  echo "<option value=$r[kd_jenisakun]>$r[desc_jenisakun]</option>";
										}  
									?>
						</select>
                </div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Posisi</span>
                    <select class="form-control" style="width:150px;height:40px;border-radius: 0 5px 5px 0;" name="posisi">
							<option value="Neraca">Neraca</option>
							<option value="Laba - Rugi">Laba - Rugi</option>
						</select>
                </div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Awal Debet</span>
                    <input type="number" class="form-control" placeholder="Saldo Debet" name="saldo_awal_debet" required>
                </div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Awal Kredit</span>
                    <input type="number" class="form-control" placeholder="Saldo Kredit" name="saldo_awal_kredit" required>
                </div><p></p>
				<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-check icon-white"></i>&nbsp;&nbsp;Submit</button>
						<button type="reset" class="btn btn-default"><i class="glyphicon glyphicon-refresh icon-white"></i>&nbsp;&nbsp;Reset</button>&nbsp;
				</div>
				</form>
				</div>
			</div>
			</div>
			</div><!--/.row-->
    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->
</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="../assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src="../assets/bower_components/moment/min/moment.min.js"></script>
<script src="../assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<!-- data table plugin -->
<script src="../assets/js/jquery.dataTables.min.js"></script>
<!-- select or dropdown enhancer -->
<script src="../assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="../assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="../assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="../assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="../assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="../assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="../assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="../assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="../assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="../assets/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="../assets/js/charisma.js"></script>
<script src="../assets/js/bootstrap-table.js"></script>
</body>
</html>
